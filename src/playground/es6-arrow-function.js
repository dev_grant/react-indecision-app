const square = function (x) {
  return x * x;
};

console.log(square(8));

// const squareArrow = (x) => {
//   return x * x;
// };

// expression syntax 
const squareArrow = (x) => x * x;

console.log(squareArrow(3))

const getFirstName = (fullName) => {
  return fullName.split(' ')[0];
}
// const getFirstName = (fullName) => fullName.split(' ')[0];

console.log(getFirstName('Grant Best'));
