// arguments objects - no longer bound with arrow functions, if you need accsess to the arguments use es5 function

const add = function (a, b) {
  console.log(arguments);
  return a + b;

}; 
// console.log(add(55, 1, 1001));

// this keyword - no longer bound 

const user = {
  name: 'Grant', 
  cities: ['Atlanta', 'Ventura', 'Salt Lake City'], 
  printPlacesLived() {
    return this.cities.map((city) => this.name + ' has lived in ' + city);
  }
};
console.log(user.printPlacesLived());

//Challenge area

const multiplier = {
  numbers: [1, 2, 3], 
  multiplyBy: 2,
  multiply() {
    return this.numbers.map((number) => number * this.multiplyBy);
  }
};
console.log(multiplier.multiply());