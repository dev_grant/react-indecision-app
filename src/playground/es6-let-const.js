var nameVar = 'Grant';
var nameVar = 'Bob';
console.log('nameVar', nameVar)

let nameLet = 'Billy';
console.log('nameLet', nameLet);

nameLet = "John";

const nameConst = 'Frank';

console.log('nameConst', nameConst);

function getPetName() {
  var petName = 'Hal';
  return petName;
}

// Block scoping

const fullName = 'Grant Best';
let firstName;

if (fullName) {
  const firstName = fullName.split(' ')[0];
  console.log(firstName);
}

console.log(firstName);