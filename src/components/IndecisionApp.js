import React from 'react';
import Header from './Header';
import AddOption from './AddOption';
import Action from './Action';
import Options from './Options';
import OptionModal from './OptionModal';

class  IndecisionApp extends React.Component {
  /* constructor removed because of babel plugin "transform-class-properties" 
  giving us new syntax for class */
  state = {
    options: [],
    selectedOption: undefined
  };
  handleDeleteOptions = () => {
    this.setState(() => ({ options: [] } ));
  };
  handleDeleteOption = (optionToRemove) => {
    this.setState((prevState) => ({
      options: prevState.options.filter((option) => optionToRemove !== option )
    }));
  };
  handlePick = () => {
    const randomNum = Math.floor(Math.random() * this.state.options.length);
    const option = this.state.options[randomNum];
    this.setState(() => ({ selectedOption: option }))
    
  };
  handleAddOption = (option) => {
    if(!option) {
      return 'Enter valid value to add item';
    } else if (this.state.options.indexOf(option) > -1) {
      return 'This option already exists';
    }
    this.setState((prevState) => ({ options: prevState.options.concat(option) }));
  };
  handleClearSelectedOption = () => {
    if (this.state.selectedOption) {
      this.setState(() => ({ selectedOption: undefined }))
    }
  }
  // Lifecycle Method (Only in class based components)
  componentDidMount = () => {
    try {
      const json = localStorage.getItem('options');
      const options = JSON.parse(json);
      if (options) {
        this.setState(() => ({ options: options }));
      }
    } catch (e) {
      console.log(e);
    }
  };
  componentDidUpdate = (prevProps, prevState) => {
    if (prevState.options.length !== this.state.options.length) {
      const json = JSON.stringify(this.state.options);
      localStorage.setItem('options', json);
      console.log('saving data')
    }
  };
  
  componentWillUnmount = () => {
  };

  render() {
    const subTitle = 'Put your life in the hands of a computer';
    return (
      <div>
        <Header subTitle={subTitle} /> 
        <div className="container">
          <Action 
          hasOptions={this.state.options.length > 0} 
          handlePick={this.handlePick} />
          <div className="widget">
            <Options 
            options={this.state.options}
            handleDeleteOptions={this.handleDeleteOptions}
            handleDeleteOption={this.handleDeleteOption} /> 
            <AddOption 
            handleAddOption={this.handleAddOption} />
          </div>
        </div>
        <OptionModal 
        selectedOption={this.state.selectedOption}
        handleClearSelectedOption={this.handleClearSelectedOption}
        /> 
      </div>
    )
  };
};

export default IndecisionApp;
